#include<iostream>
#include<iomanip>
#include<fstream>

using namespace std;

double Lagrange(double known[][2],double x,int known_pt_num,int degree);
double cal_Lagrange_coef(double known[][2],int init,int final,int num,double x);
ofstream outFile("OUTPUT.DAT",ios::out);	//set output file
int main(){
	int degree=0;
	int known_pt_num=0;
	int unknown_pt_num=0;
	double tmp_unknown=0;

	ifstream inFile("INPUT.DAT",ios::in);	//set input file
	

	inFile>>known_pt_num>>unknown_pt_num>>degree;

	double known[known_pt_num][2]={0};	//store known point

	for(int i=0;i<known_pt_num;i++){
		inFile>>known[i][0]>>known[i][1];
	}

	for(int i=0;i<unknown_pt_num;i++){
		inFile>>tmp_unknown;
		outFile<<Lagrange(known,tmp_unknown,known_pt_num,degree)<<endl;
	}

	return 0;
}

double Lagrange(double known[][2],double x,int known_pt_num,int degree){
	double rst=0;

	int locate=0;
	int init=0;
	int final=0;

	//find interval where input x locate
	while(locate<known_pt_num){
		if(x>known[locate][0])locate++;

		else if(x<known[locate][0]){
			break;
		}

		else if(x==known[locate][0]){
			return known[locate][1];
		}
	}
	
	//input x is not close to edge
	if((locate>=degree)&&(known_pt_num-locate>=degree)){
		init=locate-degree;
		final=locate+degree-1;
	}
	//input x is close to left edge
	else if(locate<degree){
		init=0;
		final=(locate+degree-1)+(degree-locate);
		if(final>=known_pt_num)final=known_pt_num-1;
	}
	//input x is close to right edge
	else if(known_pt_num-locate<degree){
		final=known_pt_num-1;
		init=(locate-degree)-(degree-(known_pt_num-locate));
		if(init<0)init=0;
	}

	for(int i=init;i<=final;i++){
		rst=rst+known[i][1]*cal_Lagrange_coef(known,init,final,i,x);
	}

	return rst;
}

double cal_Lagrange_coef(double known[][2],int init,int final,int num,double x){
	double rst=1;
	
	for(int i=init;i<=final;i++){
		if(i==num)continue;
		rst=rst*(x-known[i][0])/(known[num][0]-known[i][0]);
	}
	return rst;
}